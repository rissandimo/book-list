
// Book Constructor
function Book(title, author, isbn){
    this.title = title;
    this.author = author;
    this.isbn = isbn;
}

//UI Constructor - prototype methods for book
function UI(){}

UI.prototype.addBookToList = function(newBook){
    
    const currentBookList = document.getElementById('book-list');

    const row = document.createElement('tr');
    row.classList.add('book');
    row.innerHTML =  
    `
    <td>${newBook.title}</td>
    <td>${newBook.author}</td>
    <td>${newBook.isbn}</td>
    <td><a href="#" class="delete">X</a></td>
    `;
    
    currentBookList.appendChild(row);
};

UI.prototype.clearBookInputFields = function(){
    document.getElementById('author').value = '';
    document.getElementById('title').value = '';
    document.getElementById('isbn').value = '';
};

UI.prototype.deleteBookFromDom = function(linkTarget){

    const ui = new UI();
        /*
    target = a
    target.parent = <td>
    target.parent.parent = <tr>
    */
   if(linkTarget.classList.contains('delete')){ // delete button
    linkTarget.parentNode.parentNode.remove(); // remove row
    ui.showAlert("Book Removed", 'success');
}
}

UI.prototype.bookInfoValid = function(){
    const author = document.getElementById('author').value;
    const title = document.getElementById('title').value;
    const isbn = document.getElementById('isbn').value;

    if(author === '' || isbn === '' || title === ''){
        return false;
    }
    else{
        return true;
    }
};

UI.prototype.showAlert = function(message, className){
    const alertDiv = document.createElement('div');
    alertDiv.className = `alert ${className}`;
    alertDiv.appendChild(document.createTextNode(message));

    //get container(parent)
    const container = document.querySelector('.container');

    //get form
    const form = document.querySelector('#book-form');

    //insert alert message followed by form
    container.insertBefore(alertDiv, form);

    ///remove alert after 3 seconds
    setTimeout(function(){
        document.querySelector('.alert').remove();
    }, 3000);
};


                            //Event Listners


//add book to list                            
document.getElementById('book-form').addEventListener('submit',
function(event){
    event.preventDefault();

    //get new book form values
    const author = document.getElementById('author').value,
          isbn = document.getElementById('isbn').value,
          title = document.getElementById('title').value;
    
          //create and instantitate new book
          const newBook = new Book(title, author, isbn);

          //instantiate UI
        const ui = new UI();

        //validate book info
        if (ui.bookInfoValid()){
        //add book to list
        ui.addBookToList(newBook);
        ui.showAlert('Book added', 'success');

        //clear book form input
        ui.clearBookInputFields();
        }
        else{
       ui.showAlert('Please enter all fields', 'red');
        }

});


//remove book from list
document.querySelector('#book-list').addEventListener('click', function(event){
    event.preventDefault();
    const elementClicked = event.target; // a link

    const ui = new UI()
    ui.deleteBookFromDom(elementClicked);
});
