class Book {

    constructor(author,title, isbn){
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }
}

class UI{

    addBookToList(newBook){
        //get current book list
        const currentBookList = document.getElementById('book-list');

        //create row element
        const row = document.createElement('tr');
        row.classList.add('book');
        row.innerHTML =  
        `
        <td>${newBook.title}</td>
        <td>${newBook.author}</td>
        <td>${newBook.isbn}</td>
        <td><a href="#" class="delete" data-isbn="${isbn}">X</a></td>
        `;
        
        //add book to dom
        currentBookList.appendChild(row);
    }

    bookInfoValid(){
        const author = document.getElementById('author').value;
        const title = document.getElementById('title').value;
        const isbn = document.getElementById('isbn').value;
    
        if(author === '' || isbn === '' || title === ''){
            return false;
        }
        else{
            return true;
        }
    }

    clearBookInputFields(){
        document.getElementById('author').value = '';
        document.getElementById('title').value = '';
        document.getElementById('isbn').value = '';
    }

    deleteBookFromList(linkTarget){
        const ui = new UI();
            /*
        target = a
        target.parent = <td>
        target.parent.parent = <tr>
        */
     if(linkTarget.classList.contains('delete')){ // delete button
        linkTarget.parentNode.parentNode.remove(); // remove row
        ui.showAlert("Book Removed", 'success');
     }

    }

    showAlert(message, className){
        const alertDiv = document.createElement('div');
        alertDiv.className = `alert ${className}`;
        alertDiv.appendChild(document.createTextNode(message));
    
        //get container(parent)
        const container = document.querySelector('.container');
    
        //get form
        const form = document.querySelector('#book-form');
    
        //insert alert message followed by form
        container.insertBefore(alertDiv, form);
    
        ///remove alert after 3 seconds
        setTimeout(function(){
            document.querySelector('.alert').remove();
        }, 3000);
    }
}

// local storage
class Store{


    static saveBook(book){
        // retrieve current book
        const books = Store.retrieveBooks();

        //save book to app data
        books.push(book);

        //save book to local storage
        localStorage.setItem('books', JSON.stringify(books));

    }

    static retrieveBooks(){
        let books;

        if(localStorage.getItem('books') === null){
            books = [];
        }
        else{
            books = JSON.parse(localStorage.getItem('books'));
        }

        return books;
    }

    static removeBook(bookIsbnToDelete){
        
        //get a list of current books
        const books = Store.retrieveBooks();

        //loop through books and remove book with matching isbn
        books.forEach(function(book, index){
            if(book.isbn === bookIsbnToDelete){
                books.splice(index, 1);
            }
        });

        //save local storage
        localStorage.setItem('books', JSON.stringify(books));
    }

    static displayBooks(){
        const ui = new UI();

        //get list of books
        const books = Store.retrieveBooks()

        //traverse books and add to DOM
        books.forEach(book => ui.addBookToList(book));

    }
}



                        //Event Listners


//add book to list                            
document.getElementById('book-form').addEventListener('submit',
function(event){
    event.preventDefault();

    //get new book form values
    const author = document.getElementById('author').value,
          isbn = document.getElementById('isbn').value,
          title = document.getElementById('title').value;
    
          //create and instantitate new book
          const newBook = new Book(title, author, isbn);

          //instantiate UI
        const ui = new UI();

        //validate book info
        if (ui.bookInfoValid()){
        //add book to list
        ui.addBookToList(newBook);
        ui.showAlert('Book added', 'success');

        Store.saveBook(newBook);

        //clear book form input
        ui.clearBookInputFields();
        }
        else{
       ui.showAlert('Please enter all fields', 'red');
        }

});


//remove book from list
document.querySelector('#book-list').addEventListener('click', function(event){
    event.preventDefault();
    const elementClicked = event.target; // a link


    //remove book from UI
    const ui = new UI()
    ui.deleteBookFromList(elementClicked);

    //remove book from local storage
    // elementClicked - a link
    //elementClicked.parentElement - <li><a....></a>
    //elementClicked.parentElement.previousElementSibling - element before -> isbn <li>123456</li>
    //elementClicked.parentElement.previousElementSibling.textContent -> 123456
    Store.removeBook(elementClicked.parentElement.previousElementSibling.textContent);
});

//loads books to dom
document.addEventListener('DOMContentLoaded', Store.displayBooks);
